const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const autoprefixer = require('autoprefixer');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const { VueLoaderPlugin } = require('vue-loader');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: [
    './src/js/main.js',
    './src/css/style.css',
    './src/css/registerUL.css'
  ],
  output: {
    filename: './js/bundle_10.js',

  },
  devtool: "source-map",
  module: {
    rules: [
      {
        test: /\.vue$/,
        use: 'vue-loader'
      },

      {
        test: /\.js$/,
        include: path.resolve(__dirname, 'src/js'),
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      },

      {
        test: /\.(sass|scss|css)$/,
        include: path.resolve(__dirname, 'src/css'),
        use: ExtractTextPlugin.extract({
          use: [{
              loader: "css-loader",
              options: {
                sourceMap: true,
                minimize: true,
                url: false
              }
            },

            {
              loader: 'postcss-loader',
              options: {
                  plugins: [
                      autoprefixer({
                          browsers:['ie >= 8', 'last 4 version']
                      })
                  ],
                  sourceMap: true
              }
            },

            {
              loader: "sass-loader",
              options: {
                sourceMap: true
              }
            }
          ]
        })
      },

    ]
  },
  plugins: [
    new VueLoaderPlugin(),
    new HtmlWebpackPlugin({
      template: './src/index.html'
    }),

    new ExtractTextPlugin({
      filename: './css/style.bundle.css',
      allChunks: true,
    }),

    new CopyWebpackPlugin([
      {
        from: './src/images',
        to: './images'
      },
      
    ]),
    
  ]
};