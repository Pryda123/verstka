import Vue from 'vue';

import VeeValidate from 'vee-validate';

Vue.use(VeeValidate);

let register = new Vue({
    // el: '#form__input-ul',
    el: '#app',
    data: {
        form1: {
            lastName: '',
            firsName: '',
            middleName: '',
            email: '',
            phone: '',
            region: '',
            organizationName: '',
            organizationInn: '',
            country: '',
            index: '',
            organizationRegion: '',
            city: '',
            street: '',
            building: '',
        },
        validate: true,
        // errors: {
        //     'lastName': false,
        //     'firsName': false,
        //     'middleName': false,
        //     'email': false,
        //     'phone': false
        // },
        list_regions: [
            { region: 'Россия' },
            { region: 'США' },
            { region: 'Великобритания' },
        ],
        list_country: [
            { country: 'Россия' },
            { country: 'США' },
            { country: 'Великобритания' },
        ]
    },
    methods: {
        submit: function(e){
            e.preventDefault();
            // let err = 0;
            // let message = ''
            // // let it = this;
            
            // for(key in this.errors){
            //     if(this.form1[key] === ''){
            //         this.errors[key] = true;
            //         err = err + 1;
            //     }
            //     else this.errors[key] = false;
            // }
            // if(err) {
            //     this.validate = false;
            // } else {
            //     this.validate = true;
            //     for(input in this.form1){
            //         if(this.form1[input] !== '') message = message + this.form1[input] + ' ';
            //     }
            //     message = message.slice(0, -1);
            //     console.log(message);
            // }
        }
    }
});

document.querySelector('body').addEventListener('click', function(){
    if(document.querySelector('.form__alert')){
        register.validate = true;
    }
}, true);